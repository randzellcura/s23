let trainerAsh = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brokck", "Misty"]
    },
    talk: function () {
        console.log("Pikachu I choose you!")
    }
}

console.log(trainerAsh)

// Access properties via dot notation
console.log("Result of dot notation: ")
console.log(trainerAsh.name)

console.log("Result of dot notation: ")
console.log(trainerAsh.pokemon)

// Invoke the talk function
trainerAsh.talk()

function pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    this.tackle = function (target) {
        let targetHealth = (target.health - this.attack)
        console.log(this.name + ' tackled ' + target.name + '!');
        console.log(target.name + "'s health is now reduced to " + Number(targetHealth));
        if (targetHealth <= 0) {
            target.faint();
        }
    }
    this.faint = function(){
		console.log(this.name + " fainted.")
    }
}

let Pikachu = new pokemon("Pikachu", 12, 24, 12);
let Geodude = new pokemon("Geodude", 8, 16, 8);
let Mewtwo = new pokemon("Mewtwo", 100, 200, 100);

console.log(Pikachu)
console.log(Geodude)
console.log(Mewtwo)

Geodude.tackle(Pikachu)
Mewtwo.tackle(Geodude)
